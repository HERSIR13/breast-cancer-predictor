import 'package:flutter/material.dart';
import 'package:bcd/constants/colors.dart';

class Box extends StatelessWidget {
  final Widget child;

  Box({required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 300,
        height: 350,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.15),
            blurRadius: 40,
            offset: Offset(0, 15), // Shadow position
          ),
        ], borderRadius: BorderRadius.circular(15), color: C.white),
        child: this.child);
  }
}
