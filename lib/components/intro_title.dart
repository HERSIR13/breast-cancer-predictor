import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bcd/constants/colors.dart';
import 'package:bcd/constants/sizes.dart';

class IntroTitle extends StatelessWidget {
  final String text;

  IntroTitle({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        this.text,
        style: GoogleFonts.montserrat(
          color: C.darkBlue,
          fontSize: S.introTitle,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}
