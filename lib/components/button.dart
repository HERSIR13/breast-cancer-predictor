import 'package:bcd/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:bcd/constants/sizes.dart';
import 'package:google_fonts/google_fonts.dart';

class AppButton extends StatelessWidget {
  final String text;
  final Function onPress;
  final double minWidth;

  AppButton({required this.text, required this.onPress, required this.minWidth});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: C.shadowColor.withOpacity(0.21),
            blurRadius: 40,
            offset: Offset(15, 15), // Shadow position
          ),
        ],
      ),
      child: Material(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(21.0)),
        clipBehavior: Clip.antiAlias,
        child: Container(
          decoration: BoxDecoration(color: C.pink),
          child: MaterialButton(
            minWidth: this.minWidth,
            height: 60,
            child: Text(
              this.text,
              style: GoogleFonts.montserrat(color: C.white, fontSize: S.button, fontWeight: FontWeight.bold),
            ),
            onPressed: () => this.onPress(),
          ),
        ),
      ),
    );
  }
}
