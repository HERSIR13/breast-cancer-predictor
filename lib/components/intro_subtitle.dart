import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bcd/constants/colors.dart';
import 'package:bcd/constants/sizes.dart';

class IntroSubtitle extends StatelessWidget {
  final String text;

  IntroSubtitle({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 220,
      child: Text(
        this.text,
        style: GoogleFonts.montserrat(
          color: C.pink,
          fontSize: S.introSubtitle,
          decoration: TextDecoration.none,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
