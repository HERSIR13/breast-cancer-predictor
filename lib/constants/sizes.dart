class S {
  static final button = 18.0;
  static final introTitle = 20.0;
  static final introSubtitle = 16.0;
  static final boxText = 16.0;
  static final double input = 18.0;
  static final answer = 70.0;
}
