import 'dart:ui';

class C {
  static final pink = Color(0xFFFF6699);
  static final white = Color(0xFFFFFFFF);
  static final offWhite = Color(0xF3F3F3);
  static final darkBlue = Color(0xFF205072);
  static final shadowColor = Color(0xFF329D9C);
  static final whiteShadowColor = Color(0xCCCCC);
}
