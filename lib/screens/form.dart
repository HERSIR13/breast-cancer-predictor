import 'dart:convert';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:bcd/components/box.dart';
import 'package:bcd/components/button.dart';
import 'package:bcd/config/application.dart';
import 'package:bcd/config/questionList.dart';
import 'package:bcd/constants/colors.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bcd/constants/sizes.dart';
import 'package:http/http.dart' as http;

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  List<double> answers = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0];
  int quesIndex = 0;
  double deviceHeight = 0.0;
  Map<String, dynamic> requestBody = {};


  @override
  void initState() {
    _setInitialAnswerValues();
    _setInitialQuestionIndex();
    super.initState();
  }

  void _setInitialAnswerValues() {
    answers = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0];
  }

  void _setInitialQuestionIndex() {
    quesIndex = 0;
  }

  void _setDeviceHeight() {
    deviceHeight = MediaQuery.of(context).size.height;
  }

  @override
  Widget build(BuildContext context) {
    _setDeviceHeight();
    return WillPopScope(
      onWillPop: () => _willPopCallback(context),
      child: SafeArea(
        child: Scaffold(
          body: Ink(
            decoration: BoxDecoration(color: C.offWhite),
            child: Stack(
              children: [
                Container(
                  height: deviceHeight * 0.60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(35),
                    ),
                    color: C.pink,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: InkWell(
                            onTap: () {
                              Application.router.navigateTo(context, "/", replace: true);
                            },
                            child: Image.asset(
                              'assets/icons/arrowPrevious.png',
                              width: 15,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "Getting Started",
                      style: GoogleFonts.montserrat(color: C.white, fontSize: S.button, fontWeight: FontWeight.bold),
                    ),
                    Box(
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 32,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 40,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.0),
                                child: Text(
                                  "Specify a value for ${Questions.questionList[quesIndex]}",
                                  style: GoogleFonts.montserrat(color: C.darkBlue, fontSize: S.boxText, fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Slider(
                                        activeColor: C.pink,
                                        value: answers[quesIndex],
                                        min: 1,
                                        max: 10,
                                        divisions: 9,
                                        label: answers[quesIndex].toInt().toString(),
                                        onChanged: (double value) {
                                          setState(
                                            () {
                                              answers[quesIndex] = value;
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                                        child: Text(
                                          "selected value: ${answers[quesIndex].toInt()}",
                                          style: GoogleFonts.montserrat(color: C.darkBlue, fontSize: S.boxText, fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        (quesIndex == 0)
                            ? Container()
                            : Padding(
                                padding: const EdgeInsets.all(32.0),
                                child: AppButton(
                                  text: "Previous",
                                  minWidth: 100,
                                  onPress: () {
                                    setState(
                                      () {
                                        quesIndex--;
                                      },
                                    );
                                  },
                                ),
                              ),
                        (quesIndex == Questions.questionList.length - 1)
                            ? Container()
                            : Padding(
                                padding: const EdgeInsets.all(32.0),
                                child: AppButton(
                                  text: "Next",
                                  minWidth: 100,
                                  onPress: () {
                                    setState(
                                      () {
                                        quesIndex++;
                                      },
                                    );
                                  },
                                ),
                              ),
                      ],
                    ),
                    (quesIndex != Questions.questionList.length - 1)
                        ? Container()
                        : AppButton(
                            text: "Submit",
                            minWidth: 300,
                            onPress: _confirmSubmit,
                          ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _confirmSubmit() {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          "Please check your data before submitting",
          style: TextStyle(fontSize: 20),
        ),
        contentTextStyle: GoogleFonts.montserrat(color: C.darkBlue, fontSize: S.introSubtitle, fontWeight: FontWeight.bold),
        content: Container(
          width: 300,
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, i) => Text("${Questions.questionList[i]}: ${answers[i].toInt()}\n"),
            itemCount: Questions.questionList.length,
          ),
        ),
        actions: [
          AppButton(
            text: "Cancel",
            onPress: () async {
              Navigator.pop(context);
            },
            minWidth: 0,
          ),
          AppButton(
            text: "Confirm",
            onPress: () async {
              EasyLoading.show(status: "Predicting...");
              _checkIfDetected();
            },
            minWidth: 0,
          ),
        ],
      ),
    );
  }

  _willPopCallback(BuildContext context) {
    Application.router.navigateTo(context, "/", replace: true);
  }

  Future<void> setData() async {
    for (int i = 0; i < 9; i++) {
      requestBody.addAll({Questions.questionList[i]: answers[i].toInt()});
    }
  }

  _checkIfDetected() async {
    await setData();
    Uri uri = Uri.https("hersir.pythonanywhere.com", "/api/v1/model/predict");
    final res = await http.post(uri, body: jsonEncode(requestBody), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    if (res.statusCode == 201) {
      EasyLoading.dismiss();
      Navigator.pop(context);
        final data = jsonDecode(res.body);
        String resultString;
        if (data["result"] == 1)
          resultString = "Breast Cancer was detected";
        else
          resultString = "Breast Cancer was not detected";
        _displayAlertDialog(resultString);
      } else {
        _displayAlertDialog("Something went wrong");
        print(res.reasonPhrase);
    }
  }

  _displayAlertDialog(String messageResult) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          "Result",
          style: TextStyle(fontSize: 20),
        ),
        contentTextStyle: GoogleFonts.montserrat(color: C.darkBlue, fontSize: S.introSubtitle, fontWeight: FontWeight.bold),
        content: Container(
          child: Text(messageResult),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 20),
            child: IconButton(
              color: C.pink,
              icon: Icon(Icons.repeat),
              onPressed: () {
                setState(() {
                  quesIndex = 0;
                  _setInitialAnswerValues();
                });
                Navigator.pop(context);
              },
              tooltip: "Reset",
              iconSize: 30,
            ),
          ),
          IconButton(
            color: C.pink,
            icon: Icon(Icons.check),
            onPressed: () {
              Application.router.navigateTo(context, "/", replace: true, transition: TransitionType.inFromRight);
            },
            tooltip: "Confirm",
            iconSize: 30,
          ),
        ],
      ),
    );
  }
}
