import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bcd/components/button.dart';
import 'package:bcd/components/intro_subtitle.dart';
import 'package:bcd/components/intro_title.dart';
import 'package:bcd/config/application.dart';

class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  @override
  Widget build(BuildContext context) {
    var deviceHeight = MediaQuery.of(context).size.height;
    return WillPopScope(
      onWillPop: () => _willPopCallback(context),
      child: Scaffold(
        body: Container(
          height: deviceHeight,
          alignment: Alignment.center,
          padding: EdgeInsets.only(right: 24, left: 24, top: 40, bottom: 30),
          color: Colors.white,
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Hero(
                  tag: 'logo',
                  child: Image.asset(
                    'assets/icons/logo.png',
                    width: 70,
                  ),
                ),
                Hero(
                  tag: 'title',
                  child: IntroTitle(
                    text: 'Sponsored by Health Patrol',
                  ),
                ),
                Hero(
                  tag: 'subtitle',
                  child: IntroSubtitle(text: 'A Breast Cancer prediction tool based on the best Machine Learning methods'),
                ),
                Image.asset(
                  'assets/images/ribbon.png',
                  width: 200,
                ),
                Hero(
                  tag: 'button',
                  child: AppButton(
                    text: 'Get Started',
                    minWidth: 300.0,
                    onPress: () {
                      Application.router.navigateTo(context, "/form", replace: true);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _willPopCallback(context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Are you sure you want to quit?"),
        actions: [
          AppButton(
            text: "cancel",
            onPress: () async {
              Navigator.pop(context);
            },
            minWidth: 0,
          ),
          AppButton(
            text: "quit",
            onPress: () async {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            minWidth: 0,
          ),
        ],
      ),
    );
  }
}
