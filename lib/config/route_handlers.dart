import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:bcd/screens/intro.dart';
import 'package:bcd/screens/form.dart';

var introHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) {
  return Intro();
});
var formHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  print("form");
  return FormPage();
});
