import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:bcd/config/route_handlers.dart';

import 'route_handlers.dart';

class Routes {
  static String root = "/";
  static String form = "/form";

  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> parameters) { print("Something went wrong"); });
    router.define(root, handler: introHandler);
    router.define(form, handler: formHandler, transitionType: TransitionType.native);
  }
}
