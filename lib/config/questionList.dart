class Questions {
  static final questionList = [
    "Clump Thickness",
    "Uniformity of cell size",
    "Uniformity of cell shape",
    "Marginal adhesion",
    "Single epithelial cell size",
    "Bare nuclei",
    "Bland chromatin",
    "Normal nuclei",
    "Mitosis",
  ];
}
